## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Cisco Firepower Management Center. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Cisco Firepower Management Center.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Firepower_management_center. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">createDeviceCopyConfigRequest(domainUUID, body, callback)</td>
    <td style="padding:15px">createDeviceCopyConfigRequest</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/copyconfigrequests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllDevice(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDevice(domainUUID, body, callback)</td>
    <td style="padding:15px">createDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllFTDBridgeGroupInterface(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllFTDBridgeGroupInterface</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/bridgegroupinterfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFTDBridgeGroupInterface(containerUUID, domainUUID, body, callback)</td>
    <td style="padding:15px">createFTDBridgeGroupInterface</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/bridgegroupinterfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFTDBridgeGroupInterface(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">getFTDBridgeGroupInterface</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/bridgegroupinterfaces/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFTDBridgeGroupInterface(objectId, containerUUID, domainUUID, body, callback)</td>
    <td style="padding:15px">updateFTDBridgeGroupInterface</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/bridgegroupinterfaces/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFTDBridgeGroupInterface(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">deleteFTDBridgeGroupInterface</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/bridgegroupinterfaces/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllFTDEtherChannelInterface(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllFTDEtherChannelInterface</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/etherchannelinterfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFTDEtherChannelInterface(containerUUID, domainUUID, body, callback)</td>
    <td style="padding:15px">createFTDEtherChannelInterface</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/etherchannelinterfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFTDEtherChannelInterface(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">getFTDEtherChannelInterface</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/etherchannelinterfaces/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFTDEtherChannelInterface(objectId, containerUUID, domainUUID, body, callback)</td>
    <td style="padding:15px">updateFTDEtherChannelInterface</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/etherchannelinterfaces/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFTDEtherChannelInterface(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">deleteFTDEtherChannelInterface</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/etherchannelinterfaces/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFPInterfaceStatistics(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getFPInterfaceStatistics</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/fpinterfacestatistics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllFPLogicalInterface(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllFPLogicalInterface</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/fplogicalinterfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFPLogicalInterface(containerUUID, domainUUID, body, callback)</td>
    <td style="padding:15px">createFPLogicalInterface</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/fplogicalinterfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFPLogicalInterface(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">getFPLogicalInterface</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/fplogicalinterfaces/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFPLogicalInterface(objectId, containerUUID, domainUUID, body, callback)</td>
    <td style="padding:15px">updateFPLogicalInterface</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/fplogicalinterfaces/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFPLogicalInterface(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">deleteFPLogicalInterface</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/fplogicalinterfaces/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllFPPhysicalInterface(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllFPPhysicalInterface</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/fpphysicalinterfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFPPhysicalInterface(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">getFPPhysicalInterface</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/fpphysicalinterfaces/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFPPhysicalInterface(objectId, containerUUID, domainUUID, body, callback)</td>
    <td style="padding:15px">updateFPPhysicalInterface</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/fpphysicalinterfaces/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllInlineSet(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllInlineSet</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/inlinesets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createInlineSet(containerUUID, domainUUID, body, callback)</td>
    <td style="padding:15px">createInlineSet</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/inlinesets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInlineSet(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">getInlineSet</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/inlinesets/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateInlineSet(objectId, containerUUID, domainUUID, body, callback)</td>
    <td style="padding:15px">updateInlineSet</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/inlinesets/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteInlineSet(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">deleteInlineSet</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/inlinesets/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInterfaceEvent(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getInterfaceEvent</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/interfaceevents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createInterfaceEvent(containerUUID, domainUUID, body, callback)</td>
    <td style="padding:15px">createInterfaceEvent</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/interfaceevents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllFTDPhysicalInterface(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllFTDPhysicalInterface</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/physicalinterfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFTDPhysicalInterface(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">getFTDPhysicalInterface</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/physicalinterfaces/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFTDPhysicalInterface(objectId, containerUUID, domainUUID, body, callback)</td>
    <td style="padding:15px">updateFTDPhysicalInterface</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/physicalinterfaces/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllFTDRedundantInterface(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllFTDRedundantInterface</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/redundantinterfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFTDRedundantInterface(containerUUID, domainUUID, body, callback)</td>
    <td style="padding:15px">createFTDRedundantInterface</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/redundantinterfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFTDRedundantInterface(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">getFTDRedundantInterface</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/redundantinterfaces/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFTDRedundantInterface(objectId, containerUUID, domainUUID, body, callback)</td>
    <td style="padding:15px">updateFTDRedundantInterface</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/redundantinterfaces/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFTDRedundantInterface(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">deleteFTDRedundantInterface</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/redundantinterfaces/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllIPv4StaticRouteModel(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllIPv4StaticRouteModel</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/routing/ipv4staticroutes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIPv4StaticRouteModel(bulk, containerUUID, domainUUID, body, callback)</td>
    <td style="padding:15px">createIPv4StaticRouteModel</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/routing/ipv4staticroutes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPv4StaticRouteModel(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">getIPv4StaticRouteModel</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/routing/ipv4staticroutes/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIPv4StaticRouteModel(objectId, containerUUID, domainUUID, body, callback)</td>
    <td style="padding:15px">updateIPv4StaticRouteModel</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/routing/ipv4staticroutes/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIPv4StaticRouteModel(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">deleteIPv4StaticRouteModel</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/routing/ipv4staticroutes/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllIPv6StaticRouteModel(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllIPv6StaticRouteModel</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/routing/ipv6staticroutes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIPv6StaticRouteModel(bulk, containerUUID, domainUUID, body, callback)</td>
    <td style="padding:15px">createIPv6StaticRouteModel</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/routing/ipv6staticroutes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPv6StaticRouteModel(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">getIPv6StaticRouteModel</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/routing/ipv6staticroutes/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIPv6StaticRouteModel(objectId, containerUUID, domainUUID, body, callback)</td>
    <td style="padding:15px">updateIPv6StaticRouteModel</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/routing/ipv6staticroutes/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIPv6StaticRouteModel(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">deleteIPv6StaticRouteModel</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/routing/ipv6staticroutes/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllStaticRouteModel(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllStaticRouteModel</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/routing/staticroutes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStaticRouteModel(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">getStaticRouteModel</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/routing/staticroutes/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllFTDSubInterface(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllFTDSubInterface</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/subinterfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFTDSubInterface(bulk, containerUUID, domainUUID, body, callback)</td>
    <td style="padding:15px">createFTDSubInterface</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/subinterfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFTDSubInterface(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">getFTDSubInterface</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/subinterfaces/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFTDSubInterface(objectId, containerUUID, domainUUID, body, callback)</td>
    <td style="padding:15px">updateFTDSubInterface</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/subinterfaces/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFTDSubInterface(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">deleteFTDSubInterface</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/subinterfaces/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllVirtualSwitch(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllVirtualSwitch</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/virtualswitches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createVirtualSwitch(containerUUID, domainUUID, body, callback)</td>
    <td style="padding:15px">createVirtualSwitch</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/virtualswitches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVirtualSwitch(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">getVirtualSwitch</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/virtualswitches/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateVirtualSwitch(objectId, containerUUID, domainUUID, body, callback)</td>
    <td style="padding:15px">updateVirtualSwitch</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/virtualswitches/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVirtualSwitch(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">deleteVirtualSwitch</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}/virtualswitches/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFpmDevice(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getFpmDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDevice(objectId, domainUUID, body, callback)</td>
    <td style="padding:15px">updateDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDevice(objectId, domainUUID, callback)</td>
    <td style="padding:15px">deleteDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devices/devicerecords/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllPolicyAssignment(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllPolicyAssignment</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/assignment/policyassignments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPolicyAssignment(domainUUID, body, callback)</td>
    <td style="padding:15px">createPolicyAssignment</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/assignment/policyassignments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicyAssignment(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getPolicyAssignment</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/assignment/policyassignments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePolicyAssignment(objectId, domainUUID, body, callback)</td>
    <td style="padding:15px">updatePolicyAssignment</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/assignment/policyassignments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllFTDHADeviceContainer(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllFTDHADeviceContainer</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devicehapairs/ftddevicehapairs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFTDHADeviceContainer(domainUUID, body, callback)</td>
    <td style="padding:15px">createFTDHADeviceContainer</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devicehapairs/ftddevicehapairs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllFTDHAInterfaceMACAddresses(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllFTDHAInterfaceMACAddresses</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devicehapairs/ftddevicehapairs/{pathv2}/failoverinterfacemacaddressconfigs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFTDHAInterfaceMACAddresses(containerUUID, domainUUID, body, callback)</td>
    <td style="padding:15px">createFTDHAInterfaceMACAddresses</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devicehapairs/ftddevicehapairs/{pathv2}/failoverinterfacemacaddressconfigs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFTDHAInterfaceMACAddresses(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">getFTDHAInterfaceMACAddresses</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devicehapairs/ftddevicehapairs/{pathv2}/failoverinterfacemacaddressconfigs/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFTDHAInterfaceMACAddresses(objectId, containerUUID, domainUUID, body, callback)</td>
    <td style="padding:15px">updateFTDHAInterfaceMACAddresses</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devicehapairs/ftddevicehapairs/{pathv2}/failoverinterfacemacaddressconfigs/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFTDHAInterfaceMACAddresses(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">deleteFTDHAInterfaceMACAddresses</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devicehapairs/ftddevicehapairs/{pathv2}/failoverinterfacemacaddressconfigs/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllFTDHAMonitoredInterfaces(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllFTDHAMonitoredInterfaces</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devicehapairs/ftddevicehapairs/{pathv2}/monitoredinterfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFTDHAMonitoredInterfaces(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">getFTDHAMonitoredInterfaces</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devicehapairs/ftddevicehapairs/{pathv2}/monitoredinterfaces/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFTDHAMonitoredInterfaces(objectId, containerUUID, domainUUID, body, callback)</td>
    <td style="padding:15px">updateFTDHAMonitoredInterfaces</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devicehapairs/ftddevicehapairs/{pathv2}/monitoredinterfaces/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFTDHADeviceContainer(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getFTDHADeviceContainer</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devicehapairs/ftddevicehapairs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFTDHADeviceContainer(objectId, domainUUID, body, callback)</td>
    <td style="padding:15px">updateFTDHADeviceContainer</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devicehapairs/ftddevicehapairs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFTDHADeviceContainer(objectId, domainUUID, callback)</td>
    <td style="padding:15px">deleteFTDHADeviceContainer</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devicehapairs/ftddevicehapairs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllUpgradePackage(offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllUpgradePackage</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_platform/v1/updates/upgradepackages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllApplicableDevice(containerUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllApplicableDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_platform/v1/updates/upgradepackages/{pathv1}/applicabledevices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUpgradePackage(objectId, callback)</td>
    <td style="padding:15px">getUpgradePackage</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_platform/v1/updates/upgradepackages/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUpgradePackage(objectId, callback)</td>
    <td style="padding:15px">deleteUpgradePackage</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_platform/v1/updates/upgradepackages/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUpgrade(body, callback)</td>
    <td style="padding:15px">createUpgrade</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_platform/v1/updates/upgrades?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRESTTaxiiCollection(domainUUID, body, callback)</td>
    <td style="padding:15px">createRESTTaxiiCollection</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_tid/v1/domain/{pathv1}/taxiiconfig/collections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRESTDiscoveryInfo(domainUUID, body, callback)</td>
    <td style="padding:15px">createRESTDiscoveryInfo</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_tid/v1/domain/{pathv1}/taxiiconfig/discoveryinfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllRESTElement(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllRESTElement</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_tid/v1/domain/{pathv1}/tid/element?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRESTElement(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getRESTElement</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_tid/v1/domain/{pathv1}/tid/element/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllRESTIncident(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllRESTIncident</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_tid/v1/domain/{pathv1}/tid/incident?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRESTIncident(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getRESTIncident</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_tid/v1/domain/{pathv1}/tid/incident/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRESTIncident(objectId, domainUUID, body, callback)</td>
    <td style="padding:15px">updateRESTIncident</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_tid/v1/domain/{pathv1}/tid/incident/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRESTIncident(objectId, domainUUID, callback)</td>
    <td style="padding:15px">deleteRESTIncident</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_tid/v1/domain/{pathv1}/tid/incident/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllRESTIndicator(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllRESTIndicator</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_tid/v1/domain/{pathv1}/tid/indicator?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRESTIndicator(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getRESTIndicator</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_tid/v1/domain/{pathv1}/tid/indicator/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRESTIndicator(objectId, domainUUID, body, callback)</td>
    <td style="padding:15px">updateRESTIndicator</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_tid/v1/domain/{pathv1}/tid/indicator/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllRESTObservable(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllRESTObservable</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_tid/v1/domain/{pathv1}/tid/observable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRESTObservable(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getRESTObservable</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_tid/v1/domain/{pathv1}/tid/observable/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRESTObservable(objectId, domainUUID, body, callback)</td>
    <td style="padding:15px">updateRESTObservable</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_tid/v1/domain/{pathv1}/tid/observable/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRESTSettings(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getRESTSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_tid/v1/domain/{pathv1}/tid/settings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRESTSettings(objectId, domainUUID, body, callback)</td>
    <td style="padding:15px">updateRESTSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_tid/v1/domain/{pathv1}/tid/settings/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllRESTTidSource(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllRESTTidSource</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_tid/v1/domain/{pathv1}/tid/source?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRESTTidSource(domainUUID, body, callback)</td>
    <td style="padding:15px">createRESTTidSource</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_tid/v1/domain/{pathv1}/tid/source?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRESTTidSource(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getRESTTidSource</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_tid/v1/domain/{pathv1}/tid/source/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRESTTidSource(objectId, domainUUID, body, callback)</td>
    <td style="padding:15px">updateRESTTidSource</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_tid/v1/domain/{pathv1}/tid/source/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRESTTidSource(objectId, domainUUID, callback)</td>
    <td style="padding:15px">deleteRESTTidSource</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_tid/v1/domain/{pathv1}/tid/source/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAuditModel(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllAuditModel</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_platform/v1/domain/{pathv1}/audit/auditrecords?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditModel(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getAuditModel</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_platform/v1/domain/{pathv1}/audit/auditrecords/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllDeviceGroup(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllDeviceGroup</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devicegroups/devicegrouprecords?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDeviceGroup(domainUUID, body, callback)</td>
    <td style="padding:15px">createDeviceGroup</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devicegroups/devicegrouprecords?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceGroup(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getDeviceGroup</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devicegroups/devicegrouprecords/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceGroup(objectId, domainUUID, body, callback)</td>
    <td style="padding:15px">updateDeviceGroup</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devicegroups/devicegrouprecords/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceGroup(objectId, domainUUID, callback)</td>
    <td style="padding:15px">deleteDeviceGroup</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/devicegroups/devicegrouprecords/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllCloudEvents(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllCloudEvents</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/integration/cloudeventsconfigs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudEvents(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getCloudEvents</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/integration/cloudeventsconfigs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudEvents(objectId, domainUUID, body, callback)</td>
    <td style="padding:15px">updateCloudEvents</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/integration/cloudeventsconfigs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllExternalLookup(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllExternalLookup</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/integration/externallookups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createExternalLookup(domainUUID, body, callback)</td>
    <td style="padding:15px">createExternalLookup</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/integration/externallookups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExternalLookup(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getExternalLookup</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/integration/externallookups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateExternalLookup(objectId, domainUUID, body, callback)</td>
    <td style="padding:15px">updateExternalLookup</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/integration/externallookups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExternalLookup(objectId, domainUUID, callback)</td>
    <td style="padding:15px">deleteExternalLookup</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/integration/externallookups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllPacketAnalyzerDevice(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllPacketAnalyzerDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/integration/packetanalyzerdevices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPacketAnalyzerDevice(domainUUID, body, callback)</td>
    <td style="padding:15px">createPacketAnalyzerDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/integration/packetanalyzerdevices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPacketAnalyzerDevice(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getPacketAnalyzerDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/integration/packetanalyzerdevices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePacketAnalyzerDevice(objectId, domainUUID, body, callback)</td>
    <td style="padding:15px">updatePacketAnalyzerDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/integration/packetanalyzerdevices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePacketAnalyzerDevice(objectId, domainUUID, callback)</td>
    <td style="padding:15px">deletePacketAnalyzerDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/integration/packetanalyzerdevices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTaskStatus(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getTaskStatus</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/job/taskstatuses/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllFTDClusterDeviceContainer(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllFTDClusterDeviceContainer</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/deviceclusters/ftddevicecluster?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFTDClusterDeviceContainer(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getFTDClusterDeviceContainer</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/deviceclusters/ftddevicecluster/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllServerVersion(offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllServerVersion</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_platform/v1/info/serverversion?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServerVersion(objectId, callback)</td>
    <td style="padding:15px">getServerVersion</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_platform/v1/info/serverversion/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAccessPolicy(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllAccessPolicy</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/accesspolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAccessPolicy(domainUUID, body, callback)</td>
    <td style="padding:15px">createAccessPolicy</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/accesspolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAccessRule(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllAccessRule</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/accesspolicies/{pathv2}/accessrules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAccessRule(bulk, insertAfter, insertBefore, section, category, containerUUID, domainUUID, body, callback)</td>
    <td style="padding:15px">createAccessRule</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/accesspolicies/{pathv2}/accessrules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAccessRule1(bulk, containerUUID, domainUUID, body, callback)</td>
    <td style="padding:15px">updateAccessRule1</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/accesspolicies/{pathv2}/accessrules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccessRule(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">getAccessRule</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/accesspolicies/{pathv2}/accessrules/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAccessRule(objectId, containerUUID, domainUUID, body, callback)</td>
    <td style="padding:15px">updateAccessRule</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/accesspolicies/{pathv2}/accessrules/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAccessRule(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">deleteAccessRule</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/accesspolicies/{pathv2}/accessrules/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllDefaultAction(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllDefaultAction</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/accesspolicies/{pathv2}/defaultactions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDefaultAction(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">getDefaultAction</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/accesspolicies/{pathv2}/defaultactions/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDefaultAction(objectId, containerUUID, domainUUID, body, callback)</td>
    <td style="padding:15px">updateDefaultAction</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/accesspolicies/{pathv2}/defaultactions/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAccessPolicyLoggingSettingModel(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllAccessPolicyLoggingSettingModel</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/accesspolicies/{pathv2}/loggingsettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccessPolicyLoggingSettingModel(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">getAccessPolicyLoggingSettingModel</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/accesspolicies/{pathv2}/loggingsettings/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAccessPolicyLoggingSettingModel(objectId, containerUUID, domainUUID, body, callback)</td>
    <td style="padding:15px">updateAccessPolicyLoggingSettingModel</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/accesspolicies/{pathv2}/loggingsettings/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHitCount(filter, containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getHitCount</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/accesspolicies/{pathv2}/operational/hitcounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHitCount(filter, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">updateHitCount</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/accesspolicies/{pathv2}/operational/hitcounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHitCount(filter, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">deleteHitCount</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/accesspolicies/{pathv2}/operational/hitcounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccessPolicy(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getAccessPolicy</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/accesspolicies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAccessPolicy(objectId, domainUUID, body, callback)</td>
    <td style="padding:15px">updateAccessPolicy</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/accesspolicies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAccessPolicy(objectId, domainUUID, callback)</td>
    <td style="padding:15px">deleteAccessPolicy</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/accesspolicies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllFilePolicy(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllFilePolicy</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/filepolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFilePolicy(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getFilePolicy</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/filepolicies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllFTDNatPolicy(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllFTDNatPolicy</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/ftdnatpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFTDNatPolicy(domainUUID, body, callback)</td>
    <td style="padding:15px">createFTDNatPolicy</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/ftdnatpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllFTDAutoNatRule(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllFTDAutoNatRule</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/ftdnatpolicies/{pathv2}/autonatrules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFTDAutoNatRule(bulk, section, containerUUID, domainUUID, body, callback)</td>
    <td style="padding:15px">createFTDAutoNatRule</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/ftdnatpolicies/{pathv2}/autonatrules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFTDAutoNatRule(objectId, section, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">getFTDAutoNatRule</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/ftdnatpolicies/{pathv2}/autonatrules/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFTDAutoNatRule(objectId, section, containerUUID, domainUUID, body, callback)</td>
    <td style="padding:15px">updateFTDAutoNatRule</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/ftdnatpolicies/{pathv2}/autonatrules/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFTDAutoNatRule(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">deleteFTDAutoNatRule</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/ftdnatpolicies/{pathv2}/autonatrules/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllFTDManualNatRule(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllFTDManualNatRule</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/ftdnatpolicies/{pathv2}/manualnatrules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFTDManualNatRule(bulk, section, targetIndex, containerUUID, domainUUID, body, callback)</td>
    <td style="padding:15px">createFTDManualNatRule</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/ftdnatpolicies/{pathv2}/manualnatrules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFTDManualNatRule(objectId, section, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">getFTDManualNatRule</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/ftdnatpolicies/{pathv2}/manualnatrules/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFTDManualNatRule(objectId, section, targetIndex, containerUUID, domainUUID, body, callback)</td>
    <td style="padding:15px">updateFTDManualNatRule</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/ftdnatpolicies/{pathv2}/manualnatrules/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFTDManualNatRule(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">deleteFTDManualNatRule</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/ftdnatpolicies/{pathv2}/manualnatrules/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllFTDNatRule(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllFTDNatRule</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/ftdnatpolicies/{pathv2}/natrules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFTDNatRule(objectId, section, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">getFTDNatRule</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/ftdnatpolicies/{pathv2}/natrules/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFTDNatPolicy(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getFTDNatPolicy</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/ftdnatpolicies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFTDNatPolicy(objectId, domainUUID, body, callback)</td>
    <td style="padding:15px">updateFTDNatPolicy</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/ftdnatpolicies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFTDNatPolicy(objectId, domainUUID, callback)</td>
    <td style="padding:15px">deleteFTDNatPolicy</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/ftdnatpolicies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllFTDS2SVpnModel(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllFTDS2SVpnModel</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/ftds2svpns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFTDS2SVpnModel(domainUUID, body, callback)</td>
    <td style="padding:15px">createFTDS2SVpnModel</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/ftds2svpns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllVpnAdvancedSettings(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllVpnAdvancedSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/ftds2svpns/{pathv2}/advancedsettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVpnAdvancedSettings(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">getVpnAdvancedSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/ftds2svpns/{pathv2}/advancedsettings/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateVpnAdvancedSettings(objectId, containerUUID, domainUUID, body, callback)</td>
    <td style="padding:15px">updateVpnAdvancedSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/ftds2svpns/{pathv2}/advancedsettings/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllVpnEndpoint(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllVpnEndpoint</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/ftds2svpns/{pathv2}/endpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createVpnEndpoint(containerUUID, domainUUID, body, callback)</td>
    <td style="padding:15px">createVpnEndpoint</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/ftds2svpns/{pathv2}/endpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVpnEndpoint(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">getVpnEndpoint</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/ftds2svpns/{pathv2}/endpoints/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateVpnEndpoint(objectId, containerUUID, domainUUID, body, callback)</td>
    <td style="padding:15px">updateVpnEndpoint</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/ftds2svpns/{pathv2}/endpoints/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVpnEndpoint(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">deleteVpnEndpoint</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/ftds2svpns/{pathv2}/endpoints/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllVpnIkeSettings(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllVpnIkeSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/ftds2svpns/{pathv2}/ikesettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVpnIkeSettings(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">getVpnIkeSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/ftds2svpns/{pathv2}/ikesettings/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateVpnIkeSettings(objectId, containerUUID, domainUUID, body, callback)</td>
    <td style="padding:15px">updateVpnIkeSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/ftds2svpns/{pathv2}/ikesettings/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllVpnIPSecSettings(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllVpnIPSecSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/ftds2svpns/{pathv2}/ipsecsettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVpnIPSecSettings(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">getVpnIPSecSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/ftds2svpns/{pathv2}/ipsecsettings/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateVpnIPSecSettings(objectId, containerUUID, domainUUID, body, callback)</td>
    <td style="padding:15px">updateVpnIPSecSettings</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/ftds2svpns/{pathv2}/ipsecsettings/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFTDS2SVpnModel(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getFTDS2SVpnModel</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/ftds2svpns/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFTDS2SVpnModel(objectId, domainUUID, body, callback)</td>
    <td style="padding:15px">updateFTDS2SVpnModel</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/ftds2svpns/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFTDS2SVpnModel(objectId, domainUUID, callback)</td>
    <td style="padding:15px">deleteFTDS2SVpnModel</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/ftds2svpns/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllIntrusionPolicy(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllIntrusionPolicy</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/intrusionpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIntrusionPolicy(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getIntrusionPolicy</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/intrusionpolicies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllPrefilterPolicy(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllPrefilterPolicy</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/prefilterpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPrefilterHitCount(filter, containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getPrefilterHitCount</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/prefilterpolicies/{pathv2}/operational/hitcounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePrefilterHitCount(filter, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">updatePrefilterHitCount</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/prefilterpolicies/{pathv2}/operational/hitcounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePrefilterHitCount(filter, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">deletePrefilterHitCount</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/prefilterpolicies/{pathv2}/operational/hitcounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPrefilterPolicy(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getPrefilterPolicy</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/prefilterpolicies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllSNMPConfig(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllSNMPConfig</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/snmpalerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSNMPConfig(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getSNMPConfig</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/snmpalerts/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllSyslogConfig(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllSyslogConfig</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/syslogalerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSyslogConfig(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getSyslogConfig</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/policy/syslogalerts/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAnyProtocolPortObject(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllAnyProtocolPortObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/anyprotocolportobjects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAnyProtocolPortObject(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getAnyProtocolPortObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/anyprotocolportobjects/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllApplicationCategory(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllApplicationCategory</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/applicationcategories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationCategory(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getApplicationCategory</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/applicationcategories/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllApplicationFilter(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllApplicationFilter</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/applicationfilters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationFilter(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getApplicationFilter</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/applicationfilters/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllApplicationProductivity(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllApplicationProductivity</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/applicationproductivities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationProductivity(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getApplicationProductivity</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/applicationproductivities/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllApplicationRisk(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllApplicationRisk</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/applicationrisks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationRisk(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getApplicationRisk</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/applicationrisks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllApplication(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllApplication</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/applications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplication(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getApplication</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/applications/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllApplicationTag(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllApplicationTag</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/applicationtags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationTag(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getApplicationTag</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/applicationtags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllApplicationType(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllApplicationType</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/applicationtypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationType(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getApplicationType</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/applicationtypes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllVpnPKIEnrollmentModel(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllVpnPKIEnrollmentModel</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/certenrollments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVpnPKIEnrollmentModel(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getVpnPKIEnrollmentModel</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/certenrollments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllContinentObject(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllContinentObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/continents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getContinentObject(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getContinentObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/continents/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllCountryObject(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllCountryObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/countries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCountryObject(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getCountryObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/countries/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllDNSServerGroupObject(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllDNSServerGroupObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/dnsservergroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDNSServerGroupObject(domainUUID, body, callback)</td>
    <td style="padding:15px">createDNSServerGroupObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/dnsservergroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDNSServerGroupObject(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getDNSServerGroupObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/dnsservergroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDNSServerGroupObject(objectId, domainUUID, body, callback)</td>
    <td style="padding:15px">updateDNSServerGroupObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/dnsservergroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDNSServerGroupObject(objectId, domainUUID, callback)</td>
    <td style="padding:15px">deleteDNSServerGroupObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/dnsservergroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllEndPointDeviceType(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllEndPointDeviceType</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/endpointdevicetypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEndPointDeviceType(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getEndPointDeviceType</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/endpointdevicetypes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllExtendedAccessListModel(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllExtendedAccessListModel</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/extendedaccesslist?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtendedAccessListModel(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getExtendedAccessListModel</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/extendedaccesslist/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllFQDNObject(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllFQDNObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/fqdns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFQDNObject(bulk, domainUUID, body, callback)</td>
    <td style="padding:15px">createFQDNObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/fqdns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllFQDNOverride(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllFQDNOverride</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/fqdns/{pathv2}/overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFQDNOverride(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">getFQDNOverride</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/fqdns/{pathv2}/overrides/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFQDNObject(objectId, overrideTargetId, domainUUID, callback)</td>
    <td style="padding:15px">getFQDNObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/fqdns/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFQDNObject(objectId, domainUUID, body, callback)</td>
    <td style="padding:15px">updateFQDNObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/fqdns/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFQDNObject(objectId, domainUUID, callback)</td>
    <td style="padding:15px">deleteFQDNObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/fqdns/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllGeoLocationObject(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllGeoLocationObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/geolocations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGeoLocationObject(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getGeoLocationObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/geolocations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllHostObject(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllHostObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/hosts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHostObject(bulk, domainUUID, body, callback)</td>
    <td style="padding:15px">createHostObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/hosts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllHostOverride(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllHostOverride</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/hosts/{pathv2}/overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHostOverride(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">getHostOverride</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/hosts/{pathv2}/overrides/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHostObject(objectId, overrideTargetId, domainUUID, callback)</td>
    <td style="padding:15px">getHostObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/hosts/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHostObject(objectId, domainUUID, body, callback)</td>
    <td style="padding:15px">updateHostObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/hosts/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHostObject(objectId, domainUUID, callback)</td>
    <td style="padding:15px">deleteHostObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/hosts/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllICMPV4Object(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllICMPV4Object</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/icmpv4objects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createICMPV4Object(bulk, domainUUID, body, callback)</td>
    <td style="padding:15px">createICMPV4Object</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/icmpv4objects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllICMPV4ObjectOverride(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllICMPV4ObjectOverride</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/icmpv4objects/{pathv2}/overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getICMPV4ObjectOverride(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">getICMPV4ObjectOverride</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/icmpv4objects/{pathv2}/overrides/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getICMPV4Object(objectId, overrideTargetId, domainUUID, callback)</td>
    <td style="padding:15px">getICMPV4Object</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/icmpv4objects/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateICMPV4Object(objectId, domainUUID, body, callback)</td>
    <td style="padding:15px">updateICMPV4Object</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/icmpv4objects/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteICMPV4Object(objectId, domainUUID, callback)</td>
    <td style="padding:15px">deleteICMPV4Object</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/icmpv4objects/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllICMPV6Object(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllICMPV6Object</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/icmpv6objects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createICMPV6Object(bulk, domainUUID, body, callback)</td>
    <td style="padding:15px">createICMPV6Object</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/icmpv6objects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllICMPV6ObjectOverride(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllICMPV6ObjectOverride</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/icmpv6objects/{pathv2}/overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getICMPV6ObjectOverride(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">getICMPV6ObjectOverride</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/icmpv6objects/{pathv2}/overrides/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getICMPV6Object(objectId, overrideTargetId, domainUUID, callback)</td>
    <td style="padding:15px">getICMPV6Object</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/icmpv6objects/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateICMPV6Object(objectId, domainUUID, body, callback)</td>
    <td style="padding:15px">updateICMPV6Object</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/icmpv6objects/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteICMPV6Object(objectId, domainUUID, callback)</td>
    <td style="padding:15px">deleteICMPV6Object</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/icmpv6objects/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllIKEv1IPsecProposal(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllIKEv1IPsecProposal</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/ikev1ipsecproposals?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIKEv1IPsecProposal(domainUUID, body, callback)</td>
    <td style="padding:15px">createIKEv1IPsecProposal</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/ikev1ipsecproposals?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIKEv1IPsecProposal(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getIKEv1IPsecProposal</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/ikev1ipsecproposals/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIKEv1IPsecProposal(objectId, domainUUID, body, callback)</td>
    <td style="padding:15px">updateIKEv1IPsecProposal</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/ikev1ipsecproposals/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIKEv1IPsecProposal(objectId, domainUUID, callback)</td>
    <td style="padding:15px">deleteIKEv1IPsecProposal</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/ikev1ipsecproposals/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllIkev1PolicyObject(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllIkev1PolicyObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/ikev1policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIkev1PolicyObject(domainUUID, body, callback)</td>
    <td style="padding:15px">createIkev1PolicyObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/ikev1policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIkev1PolicyObject(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getIkev1PolicyObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/ikev1policies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIkev1PolicyObject(objectId, domainUUID, body, callback)</td>
    <td style="padding:15px">updateIkev1PolicyObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/ikev1policies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIkev1PolicyObject(objectId, domainUUID, callback)</td>
    <td style="padding:15px">deleteIkev1PolicyObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/ikev1policies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllIKEv2IPsecProposal(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllIKEv2IPsecProposal</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/ikev2ipsecproposals?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIKEv2IPsecProposal(domainUUID, body, callback)</td>
    <td style="padding:15px">createIKEv2IPsecProposal</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/ikev2ipsecproposals?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIKEv2IPsecProposal(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getIKEv2IPsecProposal</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/ikev2ipsecproposals/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIKEv2IPsecProposal(objectId, domainUUID, body, callback)</td>
    <td style="padding:15px">updateIKEv2IPsecProposal</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/ikev2ipsecproposals/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIKEv2IPsecProposal(objectId, domainUUID, callback)</td>
    <td style="padding:15px">deleteIKEv2IPsecProposal</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/ikev2ipsecproposals/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllIkev2PolicyObject(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllIkev2PolicyObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/ikev2policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIkev2PolicyObject(domainUUID, body, callback)</td>
    <td style="padding:15px">createIkev2PolicyObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/ikev2policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIkev2PolicyObject(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getIkev2PolicyObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/ikev2policies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIkev2PolicyObject(objectId, domainUUID, body, callback)</td>
    <td style="padding:15px">updateIkev2PolicyObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/ikev2policies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIkev2PolicyObject(objectId, domainUUID, callback)</td>
    <td style="padding:15px">deleteIkev2PolicyObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/ikev2policies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllInterfaceGroupObject(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllInterfaceGroupObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/interfacegroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createInterfaceGroupObject(bulk, domainUUID, body, callback)</td>
    <td style="padding:15px">createInterfaceGroupObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/interfacegroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInterfaceGroupObject(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getInterfaceGroupObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/interfacegroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateInterfaceGroupObject(objectId, domainUUID, body, callback)</td>
    <td style="padding:15px">updateInterfaceGroupObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/interfacegroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteInterfaceGroupObject(objectId, domainUUID, callback)</td>
    <td style="padding:15px">deleteInterfaceGroupObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/interfacegroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllInterfaceObject(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllInterfaceObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/interfaceobjects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInterfaceObject(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getInterfaceObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/interfaceobjects/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllISESecurityGroupTag(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllISESecurityGroupTag</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/isesecuritygrouptags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getISESecurityGroupTag(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getISESecurityGroupTag</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/isesecuritygrouptags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllKeyChainObject(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllKeyChainObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/keychains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createKeyChainObject(bulk, domainUUID, body, callback)</td>
    <td style="padding:15px">createKeyChainObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/keychains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllKeyChainObjectOverride(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllKeyChainObjectOverride</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/keychains/{pathv2}/overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getKeyChainObjectOverride(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">getKeyChainObjectOverride</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/keychains/{pathv2}/overrides/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getKeyChainObject(objectId, overrideTargetId, domainUUID, callback)</td>
    <td style="padding:15px">getKeyChainObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/keychains/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateKeyChainObject(objectId, domainUUID, body, callback)</td>
    <td style="padding:15px">updateKeyChainObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/keychains/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteKeyChainObject(objectId, domainUUID, callback)</td>
    <td style="padding:15px">deleteKeyChainObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/keychains/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkAddress(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getNetworkAddress</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/networkaddresses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllNetworkGroup(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllNetworkGroup</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/networkgroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetworkGroup(bulk, domainUUID, body, callback)</td>
    <td style="padding:15px">createNetworkGroup</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/networkgroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllNetworkGroupOverride(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllNetworkGroupOverride</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/networkgroups/{pathv2}/overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkGroupOverride(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">getNetworkGroupOverride</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/networkgroups/{pathv2}/overrides/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkGroup(objectId, overrideTargetId, domainUUID, callback)</td>
    <td style="padding:15px">getNetworkGroup</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/networkgroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkGroup(objectId, domainUUID, body, callback)</td>
    <td style="padding:15px">updateNetworkGroup</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/networkgroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGroup(objectId, domainUUID, callback)</td>
    <td style="padding:15px">deleteNetworkGroup</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/networkgroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllNetworkObject(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllNetworkObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetworkObject(bulk, domainUUID, body, callback)</td>
    <td style="padding:15px">createNetworkObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllNetworkOverride(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllNetworkOverride</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/networks/{pathv2}/overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkObject(objectId, overrideTargetId, domainUUID, callback)</td>
    <td style="padding:15px">getNetworkObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/networks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkObject(objectId, domainUUID, body, callback)</td>
    <td style="padding:15px">updateNetworkObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/networks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkObject(objectId, domainUUID, callback)</td>
    <td style="padding:15px">deleteNetworkObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/networks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllPortObjectGroup(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllPortObjectGroup</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/portobjectgroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPortObjectGroup(bulk, domainUUID, body, callback)</td>
    <td style="padding:15px">createPortObjectGroup</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/portobjectgroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllPortObjectGroupOverride(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllPortObjectGroupOverride</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/portobjectgroups/{pathv2}/overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPortObjectGroupOverride(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">getPortObjectGroupOverride</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/portobjectgroups/{pathv2}/overrides/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPortObjectGroup(objectId, overrideTargetId, domainUUID, callback)</td>
    <td style="padding:15px">getPortObjectGroup</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/portobjectgroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePortObjectGroup(objectId, domainUUID, body, callback)</td>
    <td style="padding:15px">updatePortObjectGroup</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/portobjectgroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePortObjectGroup(objectId, domainUUID, callback)</td>
    <td style="padding:15px">deletePortObjectGroup</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/portobjectgroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPortObject(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getPortObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/ports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllProtocolPortObject(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllProtocolPortObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/protocolportobjects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createProtocolPortObject(bulk, domainUUID, body, callback)</td>
    <td style="padding:15px">createProtocolPortObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/protocolportobjects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllProtocolPortObjectOverride(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllProtocolPortObjectOverride</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/protocolportobjects/{pathv2}/overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProtocolPortObjectOverride(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">getProtocolPortObjectOverride</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/protocolportobjects/{pathv2}/overrides/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProtocolPortObject(objectId, overrideTargetId, domainUUID, callback)</td>
    <td style="padding:15px">getProtocolPortObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/protocolportobjects/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateProtocolPortObject(objectId, domainUUID, body, callback)</td>
    <td style="padding:15px">updateProtocolPortObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/protocolportobjects/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProtocolPortObject(objectId, domainUUID, callback)</td>
    <td style="padding:15px">deleteProtocolPortObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/protocolportobjects/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllRangeObject(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllRangeObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/ranges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRangeObject(bulk, domainUUID, body, callback)</td>
    <td style="padding:15px">createRangeObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/ranges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllRangeOverride(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllRangeOverride</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/ranges/{pathv2}/overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRangeOverride(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">getRangeOverride</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/ranges/{pathv2}/overrides/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRangeObject(objectId, overrideTargetId, domainUUID, callback)</td>
    <td style="padding:15px">getRangeObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/ranges/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRangeObject(objectId, domainUUID, body, callback)</td>
    <td style="padding:15px">updateRangeObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/ranges/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRangeObject(objectId, domainUUID, callback)</td>
    <td style="padding:15px">deleteRangeObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/ranges/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllRealm(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllRealm</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/realms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRealm(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getRealm</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/realms/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllRealmUserGroup(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllRealmUserGroup</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/realmusergroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRealmUserGroup(objectId, realmUuid, domainUUID, callback)</td>
    <td style="padding:15px">getRealmUserGroup</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/realmusergroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllRealmUser(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllRealmUser</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/realmusers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRealmUser(objectId, realmUuid, domainUUID, callback)</td>
    <td style="padding:15px">getRealmUser</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/realmusers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllSecurityGroupTag(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllSecurityGroupTag</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/securitygrouptags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurityGroupTag(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getSecurityGroupTag</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/securitygrouptags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllSecurityZoneObject(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllSecurityZoneObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/securityzones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSecurityZoneObject(bulk, domainUUID, body, callback)</td>
    <td style="padding:15px">createSecurityZoneObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/securityzones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurityZoneObject(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getSecurityZoneObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/securityzones/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSecurityZoneObject(objectId, domainUUID, body, callback)</td>
    <td style="padding:15px">updateSecurityZoneObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/securityzones/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSecurityZoneObject(objectId, domainUUID, callback)</td>
    <td style="padding:15px">deleteSecurityZoneObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/securityzones/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllSIURLFeed(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllSIURLFeed</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/siurlfeeds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSIURLFeed(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getSIURLFeed</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/siurlfeeds/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllSIURLList(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllSIURLList</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/siurllists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSIURLList(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getSIURLList</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/siurllists/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllSLAMonitorObjectModel(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllSLAMonitorObjectModel</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/slamonitors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSLAMonitorObjectModel(bulk, domainUUID, body, callback)</td>
    <td style="padding:15px">createSLAMonitorObjectModel</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/slamonitors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSLAMonitorObjectModel(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getSLAMonitorObjectModel</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/slamonitors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSLAMonitorObjectModel(objectId, domainUUID, body, callback)</td>
    <td style="padding:15px">updateSLAMonitorObjectModel</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/slamonitors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSLAMonitorObjectModel(objectId, domainUUID, callback)</td>
    <td style="padding:15px">deleteSLAMonitorObjectModel</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/slamonitors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllTunnelTags(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllTunnelTags</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/tunneltags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTunnelTags(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getTunnelTags</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/tunneltags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllURLCategory(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllURLCategory</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/urlcategories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getURLCategory(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getURLCategory</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/urlcategories/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllURLGroupObject(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllURLGroupObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/urlgroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createURLGroupObject(bulk, domainUUID, body, callback)</td>
    <td style="padding:15px">createURLGroupObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/urlgroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllUrlGroupOverride(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllUrlGroupOverride</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/urlgroups/{pathv2}/overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUrlGroupOverride(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">getUrlGroupOverride</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/urlgroups/{pathv2}/overrides/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getURLGroupObject(objectId, overrideTargetId, domainUUID, callback)</td>
    <td style="padding:15px">getURLGroupObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/urlgroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateURLGroupObject(objectId, domainUUID, body, callback)</td>
    <td style="padding:15px">updateURLGroupObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/urlgroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteURLGroupObject(objectId, domainUUID, callback)</td>
    <td style="padding:15px">deleteURLGroupObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/urlgroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllURLObject(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllURLObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/urls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createURLObject(bulk, domainUUID, body, callback)</td>
    <td style="padding:15px">createURLObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/urls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllUrlOverride(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllUrlOverride</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/urls/{pathv2}/overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUrlOverride(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">getUrlOverride</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/urls/{pathv2}/overrides/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getURLObject(objectId, overrideTargetId, domainUUID, callback)</td>
    <td style="padding:15px">getURLObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/urls/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateURLObject(objectId, domainUUID, body, callback)</td>
    <td style="padding:15px">updateURLObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/urls/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteURLObject(objectId, domainUUID, callback)</td>
    <td style="padding:15px">deleteURLObject</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/urls/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllVariableSet(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllVariableSet</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/variablesets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVariableSet(objectId, domainUUID, callback)</td>
    <td style="padding:15px">getVariableSet</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/variablesets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllVlanTagGroup(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllVlanTagGroup</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/vlangrouptags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createVlanTagGroup(bulk, domainUUID, body, callback)</td>
    <td style="padding:15px">createVlanTagGroup</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/vlangrouptags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllVlanTagGroupOverride(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllVlanTagGroupOverride</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/vlangrouptags/{pathv2}/overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVlanTagGroupOverride(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">getVlanTagGroupOverride</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/vlangrouptags/{pathv2}/overrides/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVlanTagGroup(objectId, overrideTargetId, domainUUID, callback)</td>
    <td style="padding:15px">getVlanTagGroup</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/vlangrouptags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateVlanTagGroup(objectId, domainUUID, body, callback)</td>
    <td style="padding:15px">updateVlanTagGroup</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/vlangrouptags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVlanTagGroup(objectId, domainUUID, callback)</td>
    <td style="padding:15px">deleteVlanTagGroup</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/vlangrouptags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllVlanTag(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllVlanTag</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/vlantags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createVlanTag(bulk, domainUUID, body, callback)</td>
    <td style="padding:15px">createVlanTag</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/vlantags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllVlanOverride(containerUUID, domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getAllVlanOverride</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/vlantags/{pathv2}/overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVlanOverride(objectId, containerUUID, domainUUID, callback)</td>
    <td style="padding:15px">getVlanOverride</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/vlantags/{pathv2}/overrides/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVlanTag(objectId, overrideTargetId, domainUUID, callback)</td>
    <td style="padding:15px">getVlanTag</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/vlantags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateVlanTag(objectId, domainUUID, body, callback)</td>
    <td style="padding:15px">updateVlanTag</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/vlantags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVlanTag(objectId, domainUUID, callback)</td>
    <td style="padding:15px">deleteVlanTag</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/object/vlantags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeployableDevice(domainUUID, offset, limit, expanded, callback)</td>
    <td style="padding:15px">getDeployableDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/deployment/deployabledevices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDeploymentRequest(domainUUID, body, callback)</td>
    <td style="padding:15px">createDeploymentRequest</td>
    <td style="padding:15px">{base_path}/{version}/api/fmc_config/v1/domain/{pathv1}/deployment/deploymentrequests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
